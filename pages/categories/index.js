import { useState, useEffect, useContext } from 'react'
import UserContext from '../../UserContext'
import { Container, Image } from 'react-bootstrap'
import AddCategory from '../../components/AddCategory'
import CategoryList from '../../components/CategoryList'
import Head from 'next/head'

export default function index() {
    const { user } = useContext(UserContext)
    const [ name, setName ] = useState('')
    const [ categories, setCategories ] = useState([])

    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        })
        .then(res => res.json())
        .then(data => {
            setName(data.firstName)
            setCategories(data.categories)
        })
    },[])

    return (
        <>
        <Head><title>Categories</title></Head>
            <AddCategory setCategories={setCategories} />
            <CategoryList categories={categories} setCategories={setCategories} /> 
        </>
    )
}